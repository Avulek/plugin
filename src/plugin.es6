module.exports = exports = function(name) {
  return (function(name) {

    // the private fields
    let loaders = new Map(),
        solvers = new Map();

    // --------------------------------------------------------------
    // Public functions

    let info = () => {
      let problems = [];
      for ( let i of loaders ) {
        let [ name, _ ] = i;
        problems.push(name);
      }

      let sols = [];

      for ( let i of solvers ) {
        let [ _, m ] = i;
        for ( let [ name, _ ] of m ) {
          sols.push(name);
        }
      }

      return { name, problems, solvers: sols };
    };

    let loader = (loader_name, loader_func) => {
      loaders.set( loader_name, loader_func );
    };

    let problem = (loader_name, data) => {
      let loader = loaders.get(loader_name);
      return {
        type:    loader_name,
        problem: loader(data)
      };
    };

    let solver = (loader_name, solver_name, solver_func) => {
      if ( ! solvers.has( loader_name ) )
        solvers.set( loader_name, new Map() );

      let the_solvers = solvers.get( loader_name );
      the_solvers.set( solver_name, solver_func );
    };

    let solutions = (problem) => {
      let solvs = solvers.get( problem.type ),
          sols = [];

      for ( let [name, func] of solvs )
        sols.push({
          type:     problem.type,
          solver:   name,
          solution: func(problem.problem)
        });

      return sols;
    };

    // --------------------------------------------------------------
    // The interface
    
    return { info, loader, problem, solver, solutions };

  })(name);
};
